package pl.edu.uwm.wmii.DamianSala.Laboratorium07.pl.imiajd.Sala;

public class Nauczyciel extends Osoba{
    private int pensja;

    public Nauczyciel(String nazwisko, int rok_urodzenia, int pensja){
        super(nazwisko, rok_urodzenia);
        this.pensja = pensja;
    }

    public String toString(){
        String wynik = super.toString();
        wynik += " " + Integer.toString(pensja);
        return wynik;
    }

    public int getPensja(){
        return this.pensja;
    }
}
