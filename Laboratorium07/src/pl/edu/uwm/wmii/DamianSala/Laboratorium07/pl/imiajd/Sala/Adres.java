package pl.edu.uwm.wmii.DamianSala.Laboratorium07.pl.imiajd.Sala;

public class Adres {
    private String ulica;
    private int numer_domu;
    private int numer_mieszkania;
    private String miasto;
    private String kod_pocztowy;

    public Adres(String ulica, int numer_domu, int numer_mieszkania, String miasto, String kod_pocztowy){
        this.ulica = ulica;
        this.numer_domu = numer_domu;
        this.numer_mieszkania = numer_mieszkania;
        this.miasto = miasto;
        this.kod_pocztowy = kod_pocztowy;
    }

    public Adres(String ul, int nr_dom, String mias, String poczta){
        ulica = ul;
        numer_domu = nr_dom;
        miasto = mias;
        kod_pocztowy = poczta;
    }

    public void poka(){
        System.out.printf("%s %s\n%s %d ",kod_pocztowy, miasto, ulica, numer_domu);
        if(numer_mieszkania > 0)
            System.out.printf("/%d", numer_mieszkania);
    }

    public boolean przed(Adres test){
        if(kod_pocztowy.equals(test.kod_pocztowy))
            return true;
        return false;
    }
}
