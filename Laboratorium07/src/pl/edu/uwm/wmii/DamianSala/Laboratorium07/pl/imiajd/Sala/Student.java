package pl.edu.uwm.wmii.DamianSala.Laboratorium07.pl.imiajd.Sala;

public class Student extends Osoba{
    private String kierunek;

    public Student(String nazwisko, int rok_urodzenia, String kierunek){
        super.(nazwisko, rok_urodzenia);
        this.kierunek = kierunek;
    }

    public String toString(){
        String wynik = super.toString();
        wynik += " " + kierunek;
        return wynik;
    }

    public String getKierunek(){
        return this.kierunek;
    }
}
