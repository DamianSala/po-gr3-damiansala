package pl.edu.uwm.wmii.DamianSala.Laboratorium07.pl.imiajd.Sala;

public class Osoba {
    private String nazwisko;
    private int rok_urodzenia;

    public Osoba(String nazwisko, int rok_urodzenia){
        this.nazwisko = nazwisko;
        this.rok_urodzenia = rok_urodzenia;
    }

    public String toString(){
        String wynik = "";
        wynik += this.nazwisko + " " + Integer.toString(this.rok_urodzenia);
        return wynik;
    }

    public String getNazwisko(){
        return this.nazwisko;
    }

    public int getRok_urodzenia(){
        return this.rok_urodzenia;
    }
}
