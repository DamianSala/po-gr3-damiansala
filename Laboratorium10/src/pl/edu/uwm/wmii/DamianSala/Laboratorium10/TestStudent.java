package pl.edu.uwm.wmii.DamianSala.Laboratorium10;

import pl.edu.uwm.wmii.DamianSala.Laboratorium10.pl.imiajd.Sala.*;
import java.util.ArrayList;
import java.util.Collections;

public class testStudent {
    public static void main(String[] args){
        ArrayList<Student> grupa = new ArrayList<Student>();
        grupa.add(new Student("Sala", 2000, 12, 10, 4.0));
        grupa.add(new Student("Sala", 2000, 3, 14, 2.3));
        grupa.add(new Student("Pogorzelski", 2000, 1, 1, 2.0));
        grupa.add(new Student("Graczyk", 2000, 1, 1, 3.5));
        grupa.add(new Student("Rohde", 2000, 7, 3, 4.5));

        Collections.sort(grupa);

        for(int i = 0; i<5; i++)
            System.out.println(grupa.get(i).toString());
    }
}
