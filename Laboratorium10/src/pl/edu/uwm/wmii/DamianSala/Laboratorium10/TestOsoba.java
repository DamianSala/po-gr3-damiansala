package pl.edu.uwm.wmii.DamianSala.Laboratorium10;

import pl.edu.uwm.wmii.DamianSala.Laboratorium10.pl.imiajd.Sala.*;
import java.util.ArrayList;
import java.util.Collections;

public class testOsoba {
    public static void main(String[] args){
        ArrayList<Osoba> grupa = new ArrayList<Osoba>();
        grupa.add(new Osoba("Sala", 2000, 12, 10));
        grupa.add(new Osoba("Sala", 2000, 3, 14));
        grupa.add(new Osoba("Pogorzelski", 2000, 1, 1));
        grupa.add(new Osoba("Graczyk", 2000, 1, 1));
        grupa.add(new Osoba("Rohde", 2000, 7, 3));

        Collections.sort(grupa);

        for(int i = 0; i<5; i++)
            System.out.println(grupa.get(i).toString());
    }
}

