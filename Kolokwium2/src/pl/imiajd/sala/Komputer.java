package pl.imiajd.sala;
import java.time.LocalDate;
import java.time.LocalDateTime;

public class Komputer implements Cloneable, Comparable<Komputer> {

    public Komputer(String nazwa,LocalDate dataProdukcji) {
        this.nazwa = nazwa;
        this.dataProdukcji = dataProdukcji;
    }
    @Override
    public int compareTo(Komputer o) {
        int compare_nazwa = this.nazwa.compareTo(o.nazwa);
        if(compare_nazwa==0){
            return this.dataProdukcji.compareTo(o.dataProdukcji);
        }
        return compare_nazwa;
    }

    @Override
    public boolean equals(Object obj)
    {
        return this.toString().equals(obj.toString());
    }

    public Komputer clone() {
        try {
            return (Komputer) super.clone();
        } catch (CloneNotSupportedException e) {
            throw new AssertionError();
        }
    }

    @Override
    public String toString()
    {
        return this.getClass().getSimpleName()+" Nazwa: "+this.nazwa+" Data produkcji: "+this.dataProdukcji+"\n";
    }


    private String nazwa;
    private LocalDate dataProdukcji;

}