package pl.imiajd.sala;

import java.time.LocalDate;
public class Laptop extends Komputer implements Comparable<Komputer>, Cloneable
{
    public Laptop(String nazwa, LocalDate dataProdukcji, boolean czyApple) {
        super(nazwa, dataProdukcji);
        this.nazwa = nazwa;
        this.dataProdukcji = dataProdukcji;
        this.czyApple = czyApple;
    }


    public int compareTo(Laptop o) {
        int compare_nazwa = this.nazwa.compareTo(o.nazwa);
        if(compare_nazwa==0){
            return this.dataProdukcji.compareTo(o.dataProdukcji);
        }
        return compare_nazwa;
    }

    private boolean czyApple;
    private String nazwa;
    private LocalDate dataProdukcji;


    @Override
    public String toString()
    {
        String apple = this.czyApple ? "Marka Apple" : "Inne";
        return this.getClass().getSimpleName()+" Nazwa: "+this.nazwa+" Data produkcji: "+this.dataProdukcji+" "+apple+"\n" ;
    }
}