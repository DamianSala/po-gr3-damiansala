package pl.edu.uwm.wmii.DamianSala.Kolokwium2;
import pl.imiajd.sala.Komputer;
import pl.imiajd.sala.Laptop;
import java.time.LocalDate;
import java.util.*;

public class Main {
    public static void main(String args[]) {

        ArrayList<Komputer> grupa = new ArrayList<>();
        grupa.add(0, new Komputer("Pog", LocalDate.of(1995, 1,1)));
        grupa.add(1, new Komputer("Pog", LocalDate.of(2005, 2,2)));
        grupa.add(2, new Komputer("Pog2", LocalDate.of(2015, 3,3)));
        grupa.add(3, new Komputer("Pog3", LocalDate.of(2015, 3,3)));
        grupa.add(4, new Komputer("Pog4", LocalDate.of(2020, 4,4)));

        System.out.println("Przed Sortowaniem :");

        for(int i = 0 ; i < 5 ; i++)
        {
            System.out.print(grupa.get(i).toString());
        }
        Collections.sort(grupa);

        System.out.println("Po Sortowaniu :");

        for(int i = 0 ; i < 5 ; i++)
        {
            System.out.print(grupa.get(i).toString());
        }

        System.out.println("\n\n");

        ArrayList<Laptop> grupaLaptopow = new ArrayList<>();
        grupaLaptopow.add(0, new Laptop("Pog", LocalDate.of(1995, 1,1),true));
        grupaLaptopow.add(1, new Laptop("Pog", LocalDate.of(2005, 2,2),true));
        grupaLaptopow.add(2, new Laptop("Pog2", LocalDate.of(2015, 3,3),true));
        grupaLaptopow.add(3, new Laptop("Pog3", LocalDate.of(2015, 3,3),true));
        grupaLaptopow.add(4, new Laptop("Pog4", LocalDate.of(2020, 4,4),false));

        System.out.println("Przed Sortowaniem :");

        for(int i = 0 ; i < 5 ; i++)
        {
            System.out.print(grupaLaptopow.get(i).toString());
        }
        Collections.sort(grupaLaptopow);

        System.out.println("Po Sortowaniu :");

        for(int i = 0 ; i < 5 ; i++)
        {
            System.out.print(grupaLaptopow.get(i).toString());
        }

        System.out.println("Zadanie 2");

        LinkedList<Komputer> Komputery = new LinkedList<Komputer>();

        Komputery.add(0, new Komputer("Pog", LocalDate.of(1995, 1,1)));
        Komputery.add(1, new Komputer("Pog", LocalDate.of(2005, 2,2)));
        Komputery.add(2, new Komputer("Pog2", LocalDate.of(2015, 3,3)));
        Komputery.add(3, new Komputer("Pog3", LocalDate.of(2015, 3,3)));
        Komputery.add(4, new Komputer("Pog4", LocalDate.of(2020, 4,4)));

        System.out.println("Przed Usunięciem");

        System.out.print(Komputery);

        Redukuj.redukuj(Komputery, 2);

        System.out.println("Po Usunięciu");

        System.out.print(Komputery);
    }
}
