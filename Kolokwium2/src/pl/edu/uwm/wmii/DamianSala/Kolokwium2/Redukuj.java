package pl.edu.uwm.wmii.DamianSala.Kolokwium2;
import java.util.LinkedList;

public class Redukuj {
    public static <T> void redukuj(LinkedList<T> komputery, int n) {
        for (int i = 0; i < komputery.size(); i += (n - 1)) {
            komputery.remove(i);
        }
        System.out.println(komputery);
    }
}
