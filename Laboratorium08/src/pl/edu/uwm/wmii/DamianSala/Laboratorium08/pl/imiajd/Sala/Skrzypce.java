package pl.edu.uwm.wmii.DamianSala.Laboratorium08.pl.imiajd.Sala;

package pl.edu.uwm.wmii.LetekAdrian.laboratorium08.pl.imiajd.Letek;

public class Skrzypce extends Instrument{
    private int dzwieczek;
    private static String[] dzwieczki = {"A", "B", "C"};

    public Skrzypce(String producent, int Year){
        super(producent, Year);
    }


    public String dzwiek(){
        String wynik = dzwieczki[dzwieczek];

        dzwieczek++;
        if(dzwieczek == 3)
            dzwieczek = 0;

        return wynik;
    }
}
