package pl.edu.uwm.wmii.DamianSala.Laboratorium08.pl.imiajd.Sala;
import java.time.LocalDate;

public class Instrument {
    private String producent;
    private LocalDate rokProdukcji;

    public Instrument(String producent, int Year){
        this.producent = producent;
        this.rokProdukcji = LocalDate.of(Year,12,10);
    }

    public String getProducent(){
        return producent;
    }

    public LocalDate getRokProdukcji() {
        return rokProdukcji;
    }

    abstract public String dzwiek();

    public boolean equals(Instrument test){
        if(rokProdukcji.equals(test.rokProdukcji) && producent.equals(test.producent)) {
            return true;
        }
        return false;
    }

    public String toString(){
        String wynik = producent;
        wynik += Integer.toString(rokProdukcji.getYear());
        return wynik;
    }


}
