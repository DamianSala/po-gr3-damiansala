package pl.edu.uwm.wmii.DamianSala.Laboratorium08;
import pl.edu.uwm.wmii.DamianSala.Laboratorium08.pl.imiajd.Sala.*;
import java.util.ArrayList;

public class TestOsoba {
    public static void main(String[] args){
        String[] imionka = {"new String", "New String"};
        Osoba test1 = new Osoba(imionka, "Maczynski", 2000, 4, 20, true);

        System.out.println(test1.getImiona());
        System.out.println(test1.getDataUrodzenia());
        System.out.println(test1.getPlec());

        Pracownik test2 = new Pracownik(imionka, "Maczynski", 2000, 4, 20, true, 2000.0, 1972, 5, 20);

        System.out.println(test2.getDataZatrudnienia());

        Student test3 = new Student(imionka, "Maczynski", 2000, 4, 20, true, "Informatyka", 4.5);

        System.out.print(test3.getSredniaOcen());
    }
}
