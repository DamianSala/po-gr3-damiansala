package pl.edu.uwm.wmii.DamianSala.Laboratorium08;
import pl.edu.uwm.wmii.DamianSala.Laboratorium08.pl.imiajd.Sala.*;
import java.util.ArrayList;

public class TestInstrumenty {
    public static void main(String[] args){
        ArrayList<Instrument> Orkiestra = new ArrayList<Instrument>();
        Orkiestra.add(new Flet("flet", 1993));
        Orkiestra.add(new Flet("flecik", 2002));
        Orkiestra.add(new Flet("fletyslaw", 1944));
        Orkiestra.add(new Fortepian("PianTanos", 1999));
        Orkiestra.add(new Skrzypce("SkrzypekNaDachu", 2000));

        for(int i = 0; i<Orkiestra.size(); i++)
            System.out.println(Orkiestra.get(i).dzwiek());

        for(int i = 0; i<Orkiestra.size(); i++)
            System.out.println(Orkiestra.get(i).toString());
    }
}
