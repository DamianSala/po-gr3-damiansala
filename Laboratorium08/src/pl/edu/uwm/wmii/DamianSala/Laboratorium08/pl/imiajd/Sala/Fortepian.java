package pl.edu.uwm.wmii.DamianSala.Laboratorium08.pl.imiajd.Sala;


public class Fortepian extends Instrument{
    private int dzwieczek;
    private static String[] dzwieczki = {"Dzwiek 1", "Dzwiek 2", "Dzwiek 3"};

    public Fortepian(String producent, int Year){
        super(producent, Year);
    }


    public String dzwiek(){
        String wynik = dzwieczki[dzwieczek];

        dzwieczek++;
        if(dzwieczek == 3)
            dzwieczek = 0;

        return wynik;
    }
}

