package pl.edu.uwm.wmii.DamianSala.laboratorium03;

public class Zadanie1 {
    public static int countChar(String str, char c){
        int wystapienia = 0;
        for(int i = 0; i<str.length(); i++)
            if(str.charAt(i) == c)
                wystapienia++;

        return wystapienia;
    }

    public static int countSubStr(String str, String subStr){
        int wystapienia = 0;
        boolean is_sub;
        for(int i = 0; i<str.length()-subStr.length()+1; i++){
            is_sub = true;
            for(int j = 0; j<subStr.length(); j++)
                if(str.charAt(i+j) != subStr.charAt(j))
                    is_sub = false;

            if(is_sub == true)
                wystapienia++;
        }

        return wystapienia;
    }

    public static String middle(String str){
        if(str.length()%2 == 1)
            return str.substring(str.length()/2, (str.length()/2)+1);
        else
            return str.substring((str.length()/2)-1, (str.length()/2)+1);
    }

    public static String repeat(String str, int n){
        String wynik = "";
        for(int i = 0; i<n; i++)
            wynik = wynik+str;

        return wynik;
    }

    public static int[] where(String str, String subStr){
        int[] tab = new int[Zadanie1.countSubStr(str, subStr)];
        int iterator = 0;

        boolean is_sub;
        for(int i = 0; i<str.length()-subStr.length()+1; i++){
            is_sub = true;
            for(int j = 0; j<subStr.length(); j++)
                if(str.charAt(i+j) != subStr.charAt(j))
                    is_sub = false;

            if(is_sub == true){
                tab[iterator] = i;
                iterator++;
            }
        }

        return tab;
    }

    public static String change(String str){
        StringBuffer buffer = new StringBuffer("");
        for(int i = 0; i<str.length(); i++){
            if((int)str.charAt(i) < 97)
                buffer.append(Character.toLowerCase(str.charAt(i)));
            else
                buffer.append(Character.toUpperCase(str.charAt(i)));
        }

        return buffer.toString();
    }

    public static String nice(String str, char sep, int interval){
        StringBuffer buffer = new StringBuffer("");
        for(int i = 0; i<str.length(); i++){
            if((str.length()-i)%interval == 0 && i != 0)
                buffer.append(sep);
            buffer.append(str.charAt(i));
        }
        buffer.append(sep);

        return buffer.toString();
    }
}
